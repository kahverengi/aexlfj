#include <stdio.h>

float absoluteValue(float x);

float squareRoot(float x);

float absoluteValue(float x) {
    if (x < 0)
        x = -x;
    return (x);
}

float squareRoot(float x) {
    const float epsilon = .00001;
    float guess = 1.0;

    while (absoluteValue((guess * guess) / x - 1.0) >= epsilon) {
        guess = (x / guess + guess) / 2.0;
    }

    return guess;
}

int main() { // exercise 8
    int discriminant, a, b, c;
    float x1, x2;

    printf("What is the value of 'a'?\n");
    scanf("%i", &a);

    printf("What is the value of 'b'?\n");
    scanf("%i", &b);

    printf("What is the value of 'c'?\n");
    scanf("%i", &c);

    discriminant = (b * b) - (4 * a * c);

    if (discriminant < 0) {
        printf("Sorry bud, the roots are imaginary\n");
    } else {
        x1 = (-b + squareRoot(discriminant)) / (2 * a);

        x2 = (-b - squareRoot(discriminant)) / (2 * a);

        printf("The roots of the equation are %f and %f\n", x1, x2);
    }

    return 0;
}