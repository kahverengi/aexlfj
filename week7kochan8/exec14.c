#include <stdio.h>

int numberOfElements, i, result = 0;

int arraySum(int integerArray[]);

int main() { // exercise 14
    printf("How many elements are in the array?\n");
    scanf("%i", &numberOfElements);

    int integerArray[numberOfElements];
    printf("What are the array elements?\n");

    for (i = 0; i < numberOfElements; ++i) {
        scanf("%i", &integerArray[i]);
    }

    result = arraySum(integerArray);

    printf("The sum of the elements of the array is %i\n", result);

    return 0;
}

int arraySum(int integerArray[]) {
    for (i = 0; i < numberOfElements; ++i)
        result += integerArray[i];

    return result;
}