#include <stdio.h>
#include <stdbool.h>

int main() { // exercise 10
    int p, i;
    bool isPrime;

    printf("What number do you want to check if it is a prime? \n");
    scanf("%i", &p);

    isPrime = true;

    if (p == 1 || p == 2 || p == 3)
        printf("1\n");

    else if (p > 3) {
        for (i = 2; isPrime && p / i >= i; ++i) {
            if (p % i == 0) {
                isPrime = false;
            }
        }

        if (isPrime == true)
            printf("1\n");
        else
            printf("0\n");

    } else
        printf("Unknown input. Why must you be so difficult?\n");

    return 0;
}
