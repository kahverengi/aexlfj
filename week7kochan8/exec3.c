#include <stdio.h>

float absoluteValue(float x);

float squareRoot(float x, float y);

float absoluteValue(float x) {
    if (x < 0)
        x = -x;
    return (x);
}

float squareRoot(float x, float y) {
    const float epsilon = y;
    float guess = 1.0;

    while (absoluteValue(guess * guess - x) >= epsilon) {
        guess = (x / guess + guess) / 2.0;
    }

    return guess;
}

int main() { // exercise 3
    float epsilon, number;

    printf("What number would you like to find the square root of?\n");
    scanf("%f", &number);
    printf("What is the epsilon value?\n");
    scanf("%f", &epsilon);

    printf("squareRoot %f = %f\n", number, squareRoot(number, epsilon));

    return 0;
}