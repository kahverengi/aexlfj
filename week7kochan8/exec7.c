#include <stdio.h>

long int x_to_the_n(int x, int n);

long int x_to_the_n(int x, int n) {
    long int result = 1;

    for (int i = 1; i <= n; ++i) {
        result *= x;
    }

    return result;
}

int main() { // exercise 7
    int x, n;
    long int result;

    printf("What integer do you want to raise to a power? \n");
    scanf("%i", &x);

    do {
        printf("To what power? \n");
        scanf("%i", &n);
    } while (n < 1);

    result = x_to_the_n(x, n);

    printf("%i to the power %i is %li\n", x, n, result);

    return 0;
}