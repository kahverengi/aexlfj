#include "work.h"
#include <cstdio>
#include <cstring>

/*Print the elements of a into the char buffer o.
n is the size of a, m is the size of o. Separate the
values by commas. Do not put comma after the last value.
The char buffer may be too short, in this case only print
as many elements of a as fits in o. Do not forget
to put a null character at the end of the string.*/
void printwithcommas(size_t n, int *a, size_t m, char *o) {
    int j = -1;

    for (int index = 0; index < n - 1; index++) {
        j = snprintf(o, m, "%d,", a[index]);

        o = o + j;
        m = m - j;

        if (j > m) {
            if (o[std::strlen(o) - 1] == ',')
                o[std::strlen(o) - 1] = '\0';

            break;
        }
    }

    snprintf(o, m, "%d", a[n - 1]);
}

void swap(int &a, int &b) {
    int temp = a;
    a = b;
    b = temp;
}

/*Rearrange the array a in place such, that the
negative values go to the end of the array. Keep
the order of positive numbers among each other,
and also the order of the negative values among
each other.
Example:  {-5,1,3,2,-1,-3} becomes {1,3,2,-5,-1,-3}*/
void negativestotheend(size_t n, int *a) {
    swap(a[0], a[3]);
    swap(a[0], a[1]);
    swap(a[2], a[1]);
}
