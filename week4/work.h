#include <stdlib.h>

/*Print the elements of a into the char buffer o.
n is the size of a, m is the size of o. Separate the
values by commas. Do not put comma after the last value.
The char buffer may be too short, in this case only print
as many elements of a as fits in o. Do not forget
to put a null character at the end of the string.*/
void printwithcommas(size_t n, int *a, size_t m, char *o);


/*Rearrange the array a in place such, that the
negative values go to the end of the array. Keep
the order of positive numbers among each other,
and also the order of the negative values among
each other.
Example:  {-5,1,3,2,-1,-3} becomes {1,3,2,-5,-1,-3}*/
void negativestotheend(size_t n, int *a);


