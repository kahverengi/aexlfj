
#include "CuTest.h"
#include "work.h"

static void PrintTest1(CuTest *tc) {
    int a[] = {-5, 1, 3, 2, -1, -3};
    char o[15];
    printwithcommas(6, a, 15, o);
    CuAssertStrEquals_Msg(tc, "PrintTest1 ", "-5,1,3,2,-1,-3", o);
}

static void PrintTest2(CuTest *tc) {
    int a[] = {-5, 1, 3, 2, -1, -3};
    char o[10];
    printwithcommas(6, a, 10, o);
    CuAssertStrEquals_Msg(tc, "PrintTest1 with shor output", "-5,1,3,2", o);
}


CuSuite *GetSuite1() {
    CuSuite *suite = CuSuiteNew();

    SUITE_ADD_TEST(suite, PrintTest1);
    SUITE_ADD_TEST(suite, PrintTest2);
    return suite;
}


static void NTETest1(CuTest *tc) {
    CuAssertStrEquals_Msg(tc, "sdfs", "ee", "ee");
    int a[] = {-5, 1, 3, 2, -1, -3};
    char o[15];
    negativestotheend(6, a);
    printwithcommas(6, a, 15, o);
    CuAssertStrEquals_Msg(tc, "NTETest1 ", "1,3,2,-5,-1,-3", o);
}


CuSuite *GetSuite2() {
    CuSuite *suite = CuSuiteNew();

    SUITE_ADD_TEST(suite, NTETest1);
    return suite;
}
