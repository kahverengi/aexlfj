
#include "CuTest.h"
#include "work.h"

static bool cmparr(size_t n1, int *a1, size_t n2, int *a2) {
    if (n1 != n2) return false;
    for (size_t i = 0; i < n1; i++) {
        if (a1[i] != a2[i]) return false;
    }
    return true;
}

static void FillwithTest1(CuTest *tc) {
    int a[] = {1, 2, 3, 4};
    int ex[] = {5, 5, 5, 4};

    fillwithconstant(3, a, 5);
    CuAssert(tc, "Fillwith", cmparr(4, ex, 4, a));
}

static void CreatewithTest1(CuTest *tc) {
    int *a;
    int ex[] = {5, 5, 5};

    a = createwithconstant(3, 5);
    CuAssertPtrNotNull(tc, a);
    CuAssert(tc, "Createwith", cmparr(3, ex, 3, a));
}

static void FillwithSeqTest1(CuTest *tc) {
    int a[] = {1, 2, 3, 4};
    int ex[] = {5, 8, 11, 4};

    fillwithsequence(3, a, 5, 3);
    CuAssert(tc, "FillwithSeq", cmparr(4, ex, 4, a));
}

static void CreatewithSeqTest1(CuTest *tc) {
    int *a;
    int ex[] = {5, 8, 11};

    a = createsequence(3, 5, 3);
    CuAssertPtrNotNull(tc, a);
    CuAssert(tc, "CreatewithSeq", cmparr(3, ex, 3, a));
}

static void DotproductTest1(CuTest *tc) {
    int a1[] = {1, 2, 3, 4};
    int a2[] = {5, 5, 5, 4};

    int res = dotproduct(4, a1, a2);
    CuAssertIntEquals_Msg(tc, "Dotproduct", 46, res);
}

static void CumsumTest1(CuTest *tc) {
    int a[] = {1, 2, 1, -3, 0, 2};
    int ex[] = {1, 3, 4, 1, 1, 3};

    cumsum(6, a);
    CuAssert(tc, "Cumsum", cmparr(6, ex, 6, a));
}

static void ReverseinTest1(CuTest *tc) {
    int a[] = {1, 2, 1, -3, 0, 2};
    int ex[] = {2, 0, -3, 1, 2, 1};

    reverseinplace(6, a);
    CuAssert(tc, "Reversein", cmparr(6, ex, 6, a));
}


static void ReversenewTest1(CuTest *tc) {
    int a[] = {1, 2, 1, -3, 0, 2};
    int *res;
    int ex[] = {2, 0, -3, 1, 2, 1};

    res = reverseinnewarray(6, a);
    CuAssertPtrNotNull(tc, res);
    CuAssert(tc, "Reversenew", cmparr(6, ex, 6, res));
}


static void PrintTest1(CuTest *tc) {
    int a[] = {-5, 1, 3, 2, -1, -3};
    char o[15];
    printwithcommas(6, a, 15, o);
    CuAssertStrEquals_Msg(tc, "PrintTest1 ", "-5,1,3,2,-1,-3", o);
}

static void PrintTest2(CuTest *tc) {
    int a[] = {-5, 1, 3, 2, -1, -3};
    char o[10];
    printwithcommas(6, a, 10, o);
    CuAssertStrEquals_Msg(tc, "PrintTest1 with shor output", "-5,1,3,2", o);
}


CuSuite *GetSuite1() {
    CuSuite *suite = CuSuiteNew();

    SUITE_ADD_TEST(suite, FillwithTest1);
    SUITE_ADD_TEST(suite, CreatewithTest1);
    SUITE_ADD_TEST(suite, FillwithSeqTest1);
    SUITE_ADD_TEST(suite, CreatewithSeqTest1);

    SUITE_ADD_TEST(suite, DotproductTest1);
    SUITE_ADD_TEST(suite, CumsumTest1);
    SUITE_ADD_TEST(suite, ReverseinTest1);
    SUITE_ADD_TEST(suite, ReversenewTest1);
/*
*/
    SUITE_ADD_TEST(suite, PrintTest1);
    SUITE_ADD_TEST(suite, PrintTest2);
    return suite;
}


static void NTETest1(CuTest *tc) {
    CuAssertStrEquals_Msg(tc, "sdfs", "ee", "ee");
    int a[] = {-5, 1, 3, 2, -1, -3};
    char o[15];
    negativestotheend(6, a);
    printwithcommas(6, a, 15, o);
    CuAssertStrEquals_Msg(tc, "NTETest1 ", "1,3,2,-5,-1,-3", o);
}


CuSuite *GetSuite2() {
    CuSuite *suite = CuSuiteNew();

    SUITE_ADD_TEST(suite, NTETest1);
    return suite;
}
