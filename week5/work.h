
#include <cstdio>
#include <stdbool.h>
#include <stdlib.h>

/**Replace all elements of a with c*/
void fillwithconstant(size_t n, int *a, int c);

/**Create with malloc a new int array of size n, and
write c into all elements. Return the newly created array.*/
int *createwithconstant(size_t n, int c);

/**Fill the array with [startval, startval+stepval, startval+2*stepval,...]*/
void fillwithsequence(size_t n, int *a, int startval, int stepval);

/**Same as fillwithsequence, but the function itself must
create the array with malloc.*/
int *createsequence(size_t n, int startval, int stepval);

/**Compute the dot product of arrays a and b. The length of theses arrays is n*/
int dotproduct(size_t n, int *a, int *b);

/**Replace the array with the cumulative sum. That is
replace each element with the sum of the original element before it
plus the element itself.  Example {1,2,1,-3,0,2} becomes {1,3,4,1,1,3}*/
void cumsum(size_t n, int *a);

/**Reverse the array in place. Example {1,2,1,-3,0,2} becomes {2,0,-3,1,2,1}*/
void reverseinplace(size_t n, int *a);

/**Create a new array with malloc, and put into it the elements of
a in reverse order. Do not modify a.*/
int *reverseinnewarray(size_t n, int *a);

void splithalf(size_t n, int *a, int *o1, int *o2);

/**Return true if a1 and a2 have the same content, i.e.
they have equal size, and a1[i]==a2[i] for all elements.
Return false otherwise.*/
bool compareintarray(size_t n1, int *a1, size_t n2, int *a2);

/**Print the elements of a into the char buffer o.
n is the size of a, m is the size of o. Separate the
values by commas. Do not put comma after the last value.
The char buffer may be too short, in this case only print
as many elements of a as fits in o. Do not forget
to put a null character at the end of the string.*/
void printwithcommas(size_t n, int *a, size_t m, char *o);

/**Rearrange the array a in place such, that the
negative values go to the end of the array. Keep
the order of positive numbers among each other,
and also the order of the negative values among
each other.
Example:  {-5,1,3,2,-1,-3} becomes {1,3,2,-5,-1,-3}*/
void negativestotheend(size_t n, int *a);
