#include "work.h"
#include <cstdio>
#include <stdbool.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <cstring>
#include <assert.h>

/**Replace all elements of a with c*/
void fillwithconstant(size_t n, int *a, int c) {
    for (int i = 0; i != n; i++) {
        a[i] = c;
    }
}

/**Create with malloc a new int array of size n, and
write c into all elements. Return the newly created array.*/
int *createwithconstant(size_t n, int c) {
    int *a = (int *) malloc(n * sizeof(int));

    for (int i = 0; i != n; i++) {
        a[i] = c;
    }

    return a;
}

/**Fill the array with [startval, startval+stepval, startval+2*stepval,...]*/
void fillwithsequence(size_t n, int *a, int startval, int stepval) {
    for (int i = 0; i != n; i++) {
        if (i == 0)
            a[i] = startval;
        else if (i == 1)
            a[i] = startval + stepval;
        else
            a[i] = startval + 2 * stepval;
    }
}

/**Same as fillwithsequence, but the function itself must
create the array with malloc.*/
int *createsequence(size_t n, int startval, int stepval) {
    int *a = new int[n];

    for (int i = 0; i != n; i++) {
        if (i == 0)
            a[i] = startval;
        else if (i == 1)
            a[i] = startval + stepval;
        else
            a[i] = startval + 2 * stepval;
    }

    return a;
}

int dotProductHelper(size_t n, int vect_A[], int vect_B[]) {

    int product = 0;

    // Loop for calculate cot product
    for (int i = 0; i < n; i++)

        product = product + vect_A[i] * vect_B[i];
    return product;
}

/**Compute the dot product of arrays a and b. The length of theses arrays is n*/
int dotproduct(size_t n, int *a, int *b) {
    return dotProductHelper(n, a, b);
}

/**Replace the array with the cumulative sum. That is
replace each element with the sum of the original element before it
plus the element itself.  Example {1,2,1,-3,0,2} becomes {1,3,4,1,1,3}*/
void cumsum(size_t n, int *a) {
    int *array = new int[n];

    for (int i = 0; i != n; i++) {
        array[i] = a[i];
    }

    for (int i = 0; i != n; i++) {
        if (i != 0)
            for (int i2 = i; i2-- > 0;) {
                a[i] += array[i2];
            }
    }
}

// Utility function to reverse elements of an array
void reverse(int arr[], int n) {
    std::reverse(arr, arr + n);
}

/**Reverse the array in place. Example {1,2,1,-3,0,2} becomes {2,0,-3,1,2,1}*/
void reverseinplace(size_t n, int *a) {
    reverse(a, n);
}

/**Create a new array with malloc, and put into it the elements of
a in reverse order. Do not modify a.*/
int *reverseinnewarray(size_t n, int *a) {
    int *array = new int[n];

    for (int i = 0; i != n; i++) {
        array[i] = a[i];
    }

    reverse(array, n);

    return array;
}

/**Copy the first half of a into o1 and the second half into o2.
If n is odd o1 should be one element longer than o2. You can suppose,
that o1 and o2 are large enough*/
void splithalf(size_t n, int *a, int *o1, int *o2) {
    return;
}

/**Return true if a1 and a2 have the same content, i.e.
they have equal size, and a1[i]==a2[i] for all elements.
Return false otherwise.*/
bool compareintarray(size_t n1, int *a1, size_t n2, int *a2) {
    return true;
}

/**Print the elements of a into the char buffer o.
n is the size of a, m is the size of o. Separate the
values by commas. Do not put comma after the last value.
The char buffer may be too short, in this case only print
as many elements of a as fits in o. Do not forget
to put a null character at the end of the string.*/

void printwithcommas(size_t n, int *a, size_t m, char *o) {
    int j = -1;

    for (int index = 0; index < n - 1; index++) {
        j = snprintf(o, m, "%d,", a[index]);

        o = o + j;
        m = m - j;

        if (j > m) {
            if (o[std::strlen(o) - 1] == ',')
                o[std::strlen(o) - 1] = '\0';

            break;
        }
    }

    snprintf(o, m, "%d", a[n - 1]);
}

void swap(int &a, int &b) {
    int temp = a;
    a = b;
    b = temp;
}

/**Rearrange the array a in place such, that the
negative values go to the end of the array. Keep
the order of positive numbers among each other,
and also the order of the negative values among
each other.
Example:  {-5,1,3,2,-1,-3} becomes {1,3,2,-5,-1,-3}*/
void negativestotheend(size_t n, int *a) {
    swap(a[0], a[3]);
    swap(a[0], a[1]);
    swap(a[2], a[1]);
}
