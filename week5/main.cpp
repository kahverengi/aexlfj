#include <iostream>

#include "CuTest.h"
#include "test.h"
#include <stdio.h>
#include <stdio.h>

using namespace std;


int main() {
    CuString *output = CuStringNew();
    CuSuite *suite = CuSuiteNew();
    CuSuiteAddSuite(suite, GetSuite1());
    CuSuiteAddSuite(suite, GetSuite2());
    CuSuiteRun(suite);
    CuSuiteSummary(suite, output);
    CuSuiteDetails(suite, output);
    printf("%s\n", output->buffer);

    return 0;
}
