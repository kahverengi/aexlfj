#include <stdlib.h>

typedef struct seq_cont;

seq_cont *createEmpty();

seq_cont *createFromArray(size_t n, int *a);

size_t getSize(seq_cont *sc);

int getItem(seq_cont *sc, size_t i);

void setItem(seq_cont *sc, size_t i, int val);

void insertItem(seq_cont *sc, size_t i, int val);

void deleteItem(seq_cont *sc, size_t i);

int *toArray(seq_cont *sc);