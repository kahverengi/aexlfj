#include <stdlib.h>

#define CHUNK_SIZE 10

/**
Create a sequential container of ints using
a linked list of chunks of arrays.
Do not waste memory. When inserting, try to
use unused space in a chunk. If it is not
feasible, malloc a new chunk.
When deleting, if a chunk becomes empty, free it.*/


typedef struct seq_cont {
    size_t used; /**Used space in this chunk*/
    int elements[CHUNK_SIZE];
    struct seq_cont *next;
} seq_cont;

/**Creates a new empty container.*/
seq_cont *createEmpty() {
    return NULL;
}

/**Creates a new container which contains the integers from array a of size n.*/
seq_cont *createFromArray(size_t n, int *a) {
    return NULL;
}

/**Returns the number of items inside the container.*/
size_t getSize(seq_cont *sc) {
    int count = 0;

    for (auto x : sc->elements)
        count++;

    return count;
}

/**Returns the int at position i (starting from zero)*/
int getItem(seq_cont *sc, size_t i) {
    return -1;
}

/**Modifies the int at position i (starting from zero)*/
void setItem(seq_cont *sc, size_t i, int val) {
    return;
}

/**insets a new int at position i. The elements after it are shifted to the right*/
void insertItem(seq_cont *sc, size_t i, int val) {
    return;
}

/**deletes an int at position i. The elements after it are shifted to the left*/
void deleteItem(seq_cont *sc, size_t i) {
    return;
}

/**Returns a newly malloced array with the content of sc.*/
int *toArray(seq_cont *sc) {
    return NULL;
}

