
#include "CuTest.h"
#include "work.h"

#include <cstdio>
#include <stdbool.h>
#include <stdlib.h>
#include <iostream>
#include <vector>


static void printwithcommas(size_t n, int *a, size_t m, char *o) {
    char *q = o;
    int j;
    if (m == 10) {
        for (int i = 0; i < n - 1; i++) {
            if (i == 3) {
                j = snprintf(q, m, "%d", a[i]);
                m = m - j;
            } else {

                j = snprintf(q, m, "%d,", a[i]);
            }
            q = q + j;
            m = m - j;
        }
    } else
        for (int i = 0; i < n - 1; i++) {
            j = snprintf(q, m, "%d,", a[i]);
            q = q + j;
            m = m - j;
        }

    snprintf(q, m, "%d", a[n - 1]);
    return;
}


static bool cmparr(size_t n1, int *a1, size_t n2, int *a2) {
    if (n1 != n2) return false;
    for (size_t i = 0; i < n1; i++) {
        if (a1[i] != a2[i]) return false;
    }
    return true;
}

static void SizeTest1(CuTest *tc) {
    seq_cont *sc = createEmpty();
    size_t si = getSize(sc);
    CuAssertIntEquals_Msg(tc, "Empty size", 0, si);
    insertItem(sc, 0, 42);
    si = getSize(sc);
    CuAssertIntEquals_Msg(tc, "Size one", 1, si);
}


CuSuite *GetSuite1() {
    CuSuite *suite = CuSuiteNew();

    SUITE_ADD_TEST(suite, SizeTest1);


    return suite;
}



