#include <stdio.h>

#define    SUBTRAHEND        621049

struct dates {
    int day;
    int month;
    int year;
} dates[1];

struct dayOfWeek {
    char day[9];
} const daysOfWeek[7];

const struct dayOfWeek daysOfWeek[7] =
        {
                {"Sunday"},
                {"Monday"},
                {"Tuesday"},
                {"Wednesday"},
                {"Thursday"},
                {"Friday"},
                {"Saturday"}
        };

int main() { // exercise 4
    int i, result;
    int calculateN(struct dates d);
    int holder[1];

    for (i = 0; i < 1; ++i) {
        printf("Enter the first date in the format dd:mm:yyyy: ");
        scanf("%i:%i:%i", &dates[i].day, &dates[i].month, &dates[i].year);

        holder[i] = calculateN(dates[i]);
    }

    result = holder[0] - SUBTRAHEND;
    result %= 7;

    printf("\n");

    printf("The day of the week is = %s\n", daysOfWeek[result].day);

    printf("\n");

    return 0;
}

int calculateN(struct dates d) {
    int f, g, N;

    if (d.month <= 2) {
        f = d.year - 1;
        g = d.month + 13;
    } else {
        f = d.year;
        g = d.month + 1;
    }

    N = 1461 * f / 4 + 153 * g / 5 + d.day;

    return N;
}