#include <stdio.h>

struct dates {
    int day;
    int month;
    int year;

} dates[2];

int main() { // exercise 2
    int i, numberOfElapsedDays;
    int calculateN(struct dates d);
    int holder[2];

    for (i = 0; i < 2; ++i) {
        printf("Enter the first date in the format dd:mm:yyyy: ");
        scanf("%i:%i:%i", &dates[i].day, &dates[i].month, &dates[i].year);

        holder[i] = calculateN(dates[i]);
    }

    numberOfElapsedDays = holder[1] - holder[0];

    printf("\n");

    printf("Number of elapsed days = %i\n", numberOfElapsedDays);

    printf("\n");

    return 0;
}

int calculateN(struct dates d) {
    int f, g, N;

    if (d.month <= 2) {
        f = d.year - 1;
        g = d.month + 13;
    } else {
        f = d.year;
        g = d.month + 1;
    }

    N = 1461 * f / 4 + 153 * g / 5 + d.day;

    return N;
}
