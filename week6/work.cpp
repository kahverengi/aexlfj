#include <stdlib.h>
#include <iostream>
#include <cstring>
#include <bits/stdc++.h>

void swap_value(int *a, int *b) {
    *a = *a + *b;
    *b = *a - *b;
    *a = *a - *b;
}

/*Swap the numbers pointed by a and b */
void swap(int *a, int *b) {
    swap_value(a, b);
}

double findMedian(int a[], int n) {
    // First we sort the array
    std::sort(a, a + n);

    // check for even case
    if (n % 2 != 0)
        return (double) a[n / 2];

    return (double) (a[(n - 1) / 2] + a[n / 2]) / 2.0;
}

/*Compute the average, median, and standard deviation
of the numbers contained in the array a. (The length of a is n.)
 sttdev=sqrt(sum_i (x_i-avg)^2 /(N-1)). Return these numbers in the
respective output parameters. */
void statistics(size_t n, int *a, double *avg, double *median, double *stddev) {
    float sum = 0.0, average;

    for (int i = 0; i != n; i++) {
        sum += a[i];
    }

    *avg = sum / n;

    *median = findMedian(a, n);
}


/**Create an m by n matrix as an array of pointers to arrays of int-s. (use malloc)
   Set the value of the i-th row, j-th coloumnt to i+j */
int **prodmatrix(size_t m, size_t n) {
    int **ary = new int *[m];

    for (int i = 0; i < m; ++i) {
        ary[i] = new int[n];
    }

    int count = 1;

    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            ary[i][j] = count;

            count++;
        }
    }

    return ary;
}

/*Return the number of digits in the string. E.g. "j3g62L2" -> 4 */
int digitnumber(char *a) {
    int count = 0;

    for (int i = 0; i < strlen(a); i++)
        if (isdigit(a[i])) count++;

    return count;
}

/**Concatenate the two input string to a newly malloc-ed string.
Do not use any library function (exept for malloc).*/
char *strconcat(char *a, char *b) {
    std::string text;

    text.append(a);
    text.append(b);

    char *c = strcpy(new char[text.length() + 1], text.c_str());

    return c;
}

/**Given an array of integers, count the number of occurences
of those numbers, that occur at least once, return the found numbers
 (in ascending order) in a newly malloc-ed array pointed by *number,
and their respective occurence numbers in a newly malloc-ed array
pointed by *freq */
void frequency(size_t n, int *a, size_t *m, int **number, int **freq) {

}



/**


. [Exercise 4 Kochan] Write a program that acts as a simple “printing” calculator.The program should allow the user to type in expressions of the form
number operator
The following operators should be recognized by the program:
+ - * / S E
The S operator tells the program to set the “accumulator” to the typed-in number.
The E operator tells the program that execution is to end.The arithmetic operations
are performed on the contents of the accumulator with the number that was
keyed in acting as the second operand. The following is a “sample run” showing
how the program should operate:


 
Begin Calculations
10 S       Set Accumulator to 10
= 10.000000 Contents of Accumulator
2 /          Divide by 2
= 5.000000 Contents of Accumulator
55 -        Subtract 55
=-50.000000
100.25 S  Set Accumulator to 100.25
= 100.250000
4 *            Multiply by 4
= 401.000000
0 E            End of program
= 401.000000
End of Calculations.


*/

/**This function just performs one operation. Given the old
value of the accumulator, the given number and operation, it computes
the new value of the accumulator. Errors are not handled here.*/
double calculator_onestep(double accu, double number, char op) {
    return -1.0;
}

/**Given a series of commands, it returns a newly malloc-ed array containing
the values of the accumulator after each step. The accumulator is initially 0.0*/
double *calculator(char *commands) {
    return NULL;
}
