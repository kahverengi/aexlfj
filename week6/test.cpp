
#include "CuTest.h"
#include "work.h"

#include <cstdio>
#include <stdbool.h>
#include <stdlib.h>
#include <iostream>
#include <vector>


static void printwithcommas(size_t n, int *a, size_t m, char *o) {
    char *q = o;
    int j;
    if (m == 10) {
        for (int i = 0; i < n - 1; i++) {
            if (i == 3) {
                j = snprintf(q, m, "%d", a[i]);
                m = m - j;
            } else {

                j = snprintf(q, m, "%d,", a[i]);
            }
            q = q + j;
            m = m - j;
        }
    } else
        for (int i = 0; i < n - 1; i++) {
            j = snprintf(q, m, "%d,", a[i]);
            q = q + j;
            m = m - j;
        }

    snprintf(q, m, "%d", a[n - 1]);
    return;
}


static bool cmparr(size_t n1, int *a1, size_t n2, int *a2) {
    if (n1 != n2) return false;
    for (size_t i = 0; i < n1; i++) {
        if (a1[i] != a2[i]) return false;
    }
    return true;
}

static void SwapTest1(CuTest *tc) {
    int a = 2;
    int b = 3;
    /*You must figure out yourself, how to call swap*/
    swap(&a, &b);
    CuAssertIntEquals_Msg(tc, "swap a", 3, a);
    CuAssertIntEquals_Msg(tc, "swap a", 2, b);

}

static void StatTest1(CuTest *tc) {
    int a[] = {1, 1};
    double avg, median, stddev;

    /*You must figure out yourself, how to call swap*/
    statistics(2, a, &avg, &median, &stddev);

    CuAssertDblEquals_Msg(tc, "stat avg", 1.0, avg, 1e-10);
    CuAssertDblEquals_Msg(tc, "stat median", 1.0, median, 1e-10);
    CuAssertDblEquals_Msg(tc, "stat stddev", 0.0, stddev, 1e-10);
}

static void MatrixTest1(CuTest *tc) {
    int a[] = {1, 2, 3, 4};
    int ex[] = {5, 8, 11, 4};

    int **res = prodmatrix(2, 2);
    CuAssertPtrNotNull(tc, res);
    CuAssertIntEquals_Msg(tc, "Matrix 11", 1, res[0][0]);
    CuAssertIntEquals_Msg(tc, "Matrix 12", 2, res[0][1]);
    CuAssertIntEquals_Msg(tc, "Matrix 21", 2, res[1][0]);
    CuAssertIntEquals_Msg(tc, "Matrix 22", 4, res[1][1]);
}

static void DigitnumberTest1(CuTest *tc) {
    char *a = "j3g62L2";
    int ex = digitnumber(a);
    CuAssertIntEquals_Msg(tc, "digitnumber", 4, ex);
}

static void StrconcatTest1(CuTest *tc) {
    char *a = "j3g62L2";
    char *b = "rrrr";

    char *res = strconcat(a, b);
    CuAssertStrEquals_Msg(tc, "Strconcat", "j3g62L2rrrr", res);
}

static void FrequencyTest1(CuTest *tc) {
    int a[] = {1, 2, 1, 4, 0, 2};
    int ex1[] = {0, 1, 2, 4};
    int ex2[] = {1, 2, 2, 1};
    size_t res0;
    int *res1;
    int *res2;

    frequency(6, a, &res0, &res1, &res2);
    CuAssertIntEquals_Msg(tc, "Freq size", 4, res0);

    CuAssertPtrNotNull(tc, res1);
    CuAssertPtrNotNull(tc, res2);
    char o[15];
    printwithcommas(res0, res1, 15, o);
    CuAssertStrEquals_Msg(tc, "FreqTest1 ", "0,1,2,4", o);
    printwithcommas(res0, res2, 15, o);
    CuAssertStrEquals_Msg(tc, "FreqTest1 ", "1,2,2,1", o);

}

static void CalcOneTest1(CuTest *tc) {
    double res = calculator_onestep(2.0, 3.0, '*');
    CuAssertDblEquals_Msg(tc, "CalcOne", 6.0, res, 0.0);
}


static void CalcTest1(CuTest *tc) {
    double *res = calculator("2 S 3 * 3 + 0 E");
    CuAssertPtrNotNull(tc, res);
    CuAssertDblEquals_Msg(tc, "Calc", 2.0, res[0], 0.0);
    CuAssertDblEquals_Msg(tc, "Calc", 6.0, res[1], 0.0);
    CuAssertDblEquals_Msg(tc, "Calc", 9.0, res[2], 0.0);
    CuAssertDblEquals_Msg(tc, "Calc", 9.0, res[3], 0.0);
}


CuSuite *GetSuite1() {
    CuSuite *suite = CuSuiteNew();

    SUITE_ADD_TEST(suite, SwapTest1);
    SUITE_ADD_TEST(suite, StatTest1);
    SUITE_ADD_TEST(suite, MatrixTest1);
    SUITE_ADD_TEST(suite, DigitnumberTest1);

    SUITE_ADD_TEST(suite, StrconcatTest1);
    SUITE_ADD_TEST(suite, FrequencyTest1);
    SUITE_ADD_TEST(suite, CalcOneTest1);
    SUITE_ADD_TEST(suite, CalcTest1);

    return suite;
}



