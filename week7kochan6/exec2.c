#include <stdio.h>

int main() { // exercise 2
    int number1, number2, result;

    printf("Please provide 2 integers:\n");

    scanf("%i %i", &number1, &number2);

    result = number1 % number2;

    if (result == 0) {
        printf("%i is evenly divisible by %i\n", number1, number2);
    } else if (result != 0) {
        printf("%i is not evenly divisible by %i\n", number1, number2);
    }

    return 0;
}