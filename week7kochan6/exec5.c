#include <stdio.h>

int main() { // exercise 5
    int number, right_digit;

    printf("Enter your number.\n");
    scanf("%i", &number);

    if (number > 1) {
        do {
            right_digit = number % 10;
            printf("%i", right_digit);
            number = number / 10;

        } while (number != 0);
    } else if (number < 1) {
        number /= -1;

        printf("-");
        do {
            right_digit = number % 10;
            printf("%i", right_digit);
            number = number / 10;
        } while (number != 0);
    }

    return 0;
}