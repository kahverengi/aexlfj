#include <stdio.h>

int main() { // exercise 7
    int P[151];
    int i, j, n;

    n = 150;

    for (i = 2; i <= n; ++i) {
        P[i] = 0;
    }

    i = 2;

    while (i <= n) {
        if (P[i] == 0) {
            printf("%i is a prime\n", i);
        }

        j = 1;

        while ((i * j) <= n) {
            P[i * j] = 1;

            ++j;
        }

        ++i;
    }

    return 0;
}