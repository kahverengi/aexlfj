#include <stdio.h>

int main() { // exercise 6
    int x = 0, y = 1, i, z;

    printf("%i\n", x);
    printf("%i\n", y);

    for (int i = 0; i < 13; ++i) {
        z = x + y;
        x = y;
        y = z;
        printf("%i\n", z);
    }

    return 0;
}