#include <stdio.h>

int main() { // exercise 4
    float averageCalculatingArray[9], value, average;
    int i, sumOfValues = 0;

    for (i = 0; i < 10; ++i)
        averageCalculatingArray[i] = 0;

    sumOfValues = 0;

    for (i = 0; i < 10; ++i) {
        printf("Enter value %i:\n", i + 1);
        scanf("%f", &value);

        if (value >= 0 && value <= 999999999) {
            averageCalculatingArray[i] = value;

            sumOfValues += value;
        } else
            printf("Bad response %f\n", value);
    }

    average = (float) sumOfValues / 10;

    printf("The average is %f\n", average);

    return 0;
}