#include <stdio.h>

int main() { // exercise 2
    int values[10];
    int index;

    for (index = 0; index < 10; ++index)
        values[index] = 0;

    printf("\n\n Subscript   Element\n");
    printf("_______ ___________________\n");

    for (index = 0; index < 10; ++index)
        printf("%4i%14i\n", index, values[index]);

    return 0;
}