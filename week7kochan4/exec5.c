#include <stdio.h>

// EXPECTED OUTPUT: d = d

int main() { // exercise 5
    char c, d;

    c = 'd';
    d = c;

    printf("d = %c\n", d);

    return 0;
}