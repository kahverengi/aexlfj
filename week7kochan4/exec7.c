#include <stdio.h>

int main() { // exercise 7
    double result = (3.31e-8 * 2.01e-7) / (7.16e-6 + 2.01e-8);

    printf("Expression result is %g\n", result);

    return 0;
}