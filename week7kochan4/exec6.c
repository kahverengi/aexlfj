#include <stdio.h>

int main() { // exercise 6

    double answer = 4.0 * 2.55 * 2.55 * 2.55 - 5.0 * 2.5 * 2.5 + 6.0;

    printf("Polynomial result is %f\n", answer);

    return 0;
}
