#include <stdio.h>

int main() { // exercise 4
    double result = (27.0 - 32.0) / 1.8;

    printf("27 degrees F in C is %f\n", result);

    return 0;
}