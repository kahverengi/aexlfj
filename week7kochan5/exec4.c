#include <stdio.h>

int main() { // exercise 4
    int n, nf = 1;

    printf("TABLE OF FACTORIALS\n\n");
    printf(" n       Factorial\n");
    printf("---   --------------\n");

    for (n = 1; n < 11; n++) {
        nf *= n;

        printf("%2i          %i\n", n, nf);
    }

    return 0;
}