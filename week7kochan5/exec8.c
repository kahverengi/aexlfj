#include <stdio.h>

int main() { // exercise 8
    int n, number, triangularNumber, counter, numberOfTriangulars;

    printf("How many triangular numbers would you like\n");
    printf("to calculate?\n");
    scanf("%i", &numberOfTriangulars);

    for (counter = 1; counter <= numberOfTriangulars; ++counter) {
        printf("What triangular number do you want? ");
        scanf("%i", &number);

        triangularNumber = 0;

        for (n = 1; n <= number; ++n)
            triangularNumber += n;

        printf("Triangular number %i is %i\n\n", number, triangularNumber);
    }

    return 0;
}