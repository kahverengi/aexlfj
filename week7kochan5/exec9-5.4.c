#include <stdio.h>

int main() { // exercise 9 5.4
    int n, number, triangularNumber;

    printf("What triangular number do you want? ");
    scanf("%i", &number);

    triangularNumber = 0;

    n = 1;
    while (n <= number) {
        triangularNumber += n;
        ++n;
    }

    printf("Triangular number is %i is %i\n", number, triangularNumber);

    return 0;
}