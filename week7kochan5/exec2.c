#include <stdio.h>

int main() { // exercise 2
    int n, sqr;
    printf("TABLE OF SQUARES\n\n");
    printf("n     Square of n\n");
    printf("---   -----------\n");

    sqr = 0;

    for (n = 1; n <= 10; ++n) {
        sqr = n * n;
        printf("%2i        %i\n", n, sqr);
    }

    return 0;
}