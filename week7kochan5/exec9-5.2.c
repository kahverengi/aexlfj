#include <stdio.h>

int main() { // exercise 9 5.2
    int n, triangularNumber;

    triangularNumber = 0;

    n = 1;
    while (n <= 200) {
        triangularNumber = triangularNumber + n;
        n = n + 1;
    }

    printf("The 200th triangular mumber is %i\n", triangularNumber);

    return 0;
}